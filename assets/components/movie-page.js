import { img_base } from "../core/secrets.js"

const linkElem = document.createElement("link")
linkElem.setAttribute("rel", "stylesheet")

// TODO style better
linkElem.setAttribute("href", "assets/css/movie-page.css")

const template = document.createElement("template")
// TODO: make better template to show more content
template.innerHTML = `
  <button class="goback" id="buttonGoBack">
    Back
  </button>
  <div class="movie-page"> 
    <img id="movies-poster-path" />
    <div class="text">
      <h2> 
        <t id="movies-title">No Title</t>
      </h2>
      <h3>
        Release date: <br/>
        <t id="movies-release-date">00-00-0000</t>
      </h3>
      <h4 >
        Popularity:
        <t id="movies-popularity">0.00</t>
      </h4>
    </div>
  </div>
`

class MoviePage extends HTMLElement {
  constructor(id, title, releaseDate, popularity, posterPath) {
    super()
    this.shadow = this.attachShadow({ mode: "open" })
    this.movie_id = id
    this.title = title
    this.releaseDate = releaseDate
    this.popularity = popularity
    this.posterPath = posterPath
  }

  connectedCallback() {
    this.render()
    this.shadow
      .getElementById("buttonGoBack")
      .addEventListener("click", this.goBack)
  }

  disconnectedCallback() {
    // this.shadow.getElementById("buttonGoBack").removeEventListener("click")
  }

  render() {
    this.shadow.appendChild(linkElem.cloneNode(true))
    this.shadow.appendChild(template.content.cloneNode(true))

    this.shadow.getElementById("movies-title").innerText = this.title
    this.shadow.getElementById(
      "movies-release-date"
    ).innerText = this.releaseDate
    this.shadow.getElementById("movies-popularity").innerText = this.popularity
    this.shadow.getElementById(
      "movies-poster-path"
    ).src = `${img_base}/w500${this.posterPath}`
  }

  async goBack() {
    const main = document.getElementById("main")
    main.removeAttribute("hidden")

    document.body.removeChild(document.getElementsByTagName("movie-page")[0])
  }
}

customElements.define("movie-page", MoviePage)

export default MoviePage
