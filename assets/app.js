import { base_url, img_base, api_key } from "./core/secrets.js"
import MovieCard from "./components/movie-card.js"

const buttonSearch = document.getElementById("buttonSearch")
const main = document.getElementById("main")

buttonSearch.onclick = async (e) => {
  const query = document.getElementById("inputSearch").value
  if (query.length != 0) {
    let res = await fetch(
      `${base_url}/search/movie?api_key=${api_key}&query=${query}`
    )
    let json = await res.json()
    main.innerHTML = ""
    json.results.forEach((movie) => {
      let movieCard = new MovieCard(
        movie.id,
        movie.title,
        movie.release_date,
        movie.popularity,
        movie.poster_path
      )
      main.appendChild(movieCard)
    })
  }
}
